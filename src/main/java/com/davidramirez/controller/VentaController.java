package com.davidramirez.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.davidramirez.model.Venta;
import com.davidramirez.service.IVentaService;


@RestController
@RequestMapping("/ventas")
public class VentaController {

	
	@Autowired
	private IVentaService iVentaService;
	@GetMapping(value= "/listar",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Venta>> listar()
	{
		List<Venta> listaVenta=new ArrayList<>();
		
		try {
			listaVenta=iVentaService.listar();
		} catch (Exception e) {
			new ResponseEntity<List<Venta>>(listaVenta,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<List<Venta>>(listaVenta,HttpStatus.OK);
	}
	
	
	
	
	@PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Venta> registrar(@RequestBody Venta venta) {
		Venta c = new Venta();
		try {
			c = iVentaService.registrar(venta);
		} catch (Exception e) {
			return new ResponseEntity<Venta>(c, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Venta>(c, HttpStatus.OK);
	}


}



