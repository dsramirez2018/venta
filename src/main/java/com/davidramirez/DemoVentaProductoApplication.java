package com.davidramirez;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoVentaProductoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoVentaProductoApplication.class, args);
	}
}
