package com.davidramirez.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.davidramirez.dao.IVentaDAO;
import com.davidramirez.model.Venta;
import com.davidramirez.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService {

	@Autowired
	private IVentaDAO iVentaDAO;

	@Override
	public Venta registrar(Venta venta) {
		venta.getDetalleVenta().forEach(x -> x.setVenta(venta));	
		
		
		return iVentaDAO.save(venta);
	}

	@Override
	public void modificar(Venta venta) {
		iVentaDAO.save(venta);

	}

	@Override
	public void eliminar(int idVenta) {

		iVentaDAO.delete(idVenta);
	}

	@Override
	public Venta listarId(int idVenta) {

		return iVentaDAO.findOne(idVenta);
	}

	@Override
	public List<Venta> listar() {
		return iVentaDAO.findAll();
	}

}
