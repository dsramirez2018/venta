package com.davidramirez.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.davidramirez.dao.IPersonaDAO;
import com.davidramirez.model.Persona;
import com.davidramirez.service.IPersonaService;

@Service
public class PersonaServiceImpl  implements IPersonaService {

	@Autowired
	private IPersonaDAO iPersonaDao;
	
	
	@Override
	public Persona registrar(Persona persona) {
	
		return iPersonaDao.save(persona);
	}

	@Override
	public void modificar(Persona persona) {
	
		iPersonaDao.save(persona);
	}

	@Override
	public void eliminar(int idPersona) {
		
		iPersonaDao.delete(idPersona);
	}

	@Override
	public Persona listarId(int idPersona) {

		return iPersonaDao.findOne(idPersona);
	}

	@Override
	public List<Persona> listar() {
		
		return iPersonaDao.findAll();
	}

}
