package com.davidramirez.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.davidramirez.dao.IProductoDAO;
import com.davidramirez.model.Producto;
import com.davidramirez.service.IProductoService;


@Service
public class ProductoServiceImpl implements IProductoService{
	@Autowired
	private IProductoDAO iProductoDao;
	
	
	
	@Override
	public Producto registrar(Producto producto) {
		
		return iProductoDao.save(producto);
	}

	@Override
	public void modificar(Producto producto) {
		
		iProductoDao.save(producto);
	}

	@Override
	public void eliminar(int idProducto) {
		iProductoDao.delete(idProducto);
		
	}

	@Override
	public Producto listarId(int idProducto) {
	
		return iProductoDao.findOne(idProducto);
	}

	@Override
	public List<Producto> listar() {
		
		return iProductoDao.findAll();
	}

}
